package com.example.calculadora_java;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnSalir;
    private EditText txtUsuario;
    private EditText txtContraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
    }

    private void ingresar() {
        String strUsuario;
        String strContraseña;

        strUsuario = getResources().getString(R.string.usuario);
        strContraseña = getResources().getString(R.string.contraseña);

        if (strUsuario.equals(txtUsuario.getText().toString()) && strContraseña.equals(txtContraseña.getText().toString())) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", txtUsuario.getText().toString());

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity2.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "El usuario o contraseña no es válido", Toast.LENGTH_LONG).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Deseas salir");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {
        });
        confirmar.show();
    }
}