package com.example.calculadora_java;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity2 extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    // Declarar el objeto
    private Calculadora calculadora = new Calculadora(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora2);
        iniciarComponentes();

        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnResta();
            }
        });

        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicacion();
            }
        });

        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDivision();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRegresar();
            }
        });
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnDividir = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);

        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);
    }

    private void btnSumar() {
        if (txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Ingrese los números correspondientes", Toast.LENGTH_LONG).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.suma();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnResta() {
        if (txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Ingrese los números correspondientes", Toast.LENGTH_LONG).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.resta();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnMultiplicacion() {
        if (txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Ingrese los números correspondientes", Toast.LENGTH_LONG).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.multiplicacion();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnDivision() {
        if (txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Ingrese los números correspondientes", Toast.LENGTH_LONG).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.division();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnLimpiar() {
        lblResultado.setText("");
        txtUno.setText("");
        txtDos.setText("");
    }

    private void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {
        });
        confirmar.show();
    }
}